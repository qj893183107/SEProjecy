package org.seproject.litemall.wx.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.seproject.litemall.core.util.JacksonUtil;
import org.seproject.litemall.core.util.ResponseUtil;
import org.seproject.litemall.core.util.bcrypt.BCryptPasswordEncoder;
import org.seproject.litemall.db.domain.LitemallUser;
import org.seproject.litemall.db.service.LitemallOrderService;
import org.seproject.litemall.db.service.LitemallUserService;
import org.seproject.litemall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.seproject.litemall.wx.util.WxResponseCode.AUTH_INVALID_ACCOUNT;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户服务
 */
@RestController
@RequestMapping("/wx/user")
@Validated
public class WxUserController {
    private final Log logger = LogFactory.getLog(WxUserController.class);

    @Autowired
    private LitemallOrderService orderService;
    
    @Autowired
    private LitemallUserService userService;

    /**
     * 用户个人页面数据
     * <p>
     * 目前是用户订单统计信息
     *
     * @param userId 用户ID
     * @return 用户个人页面数据
     */
    @GetMapping("index")
    public Object list(@LoginUser Integer userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        Map<Object, Object> data = new HashMap<Object, Object>();
        data.put("order", orderService.orderInfo(userId));
        return ResponseUtil.ok(data);
    }
    
    @PostMapping("/update")
    public Object update(@RequestBody String body, HttpServletRequest request) {
        String username = JacksonUtil.parseString(body, "username");
        String password = JacksonUtil.parseString(body, "password");
        String temperature = JacksonUtil.parseString(body, "temperature");
        if (username == null || password == null) {
            return ResponseUtil.badArgument();
        }

        List<LitemallUser> userList = userService.queryByUsername(username);
        LitemallUser user = null;
        if (userList.size() > 1) {
            return ResponseUtil.serious();
        } else if (userList.size() == 0) {
            return ResponseUtil.fail(AUTH_INVALID_ACCOUNT, "账号不存在");
        } else {
            user = userList.get(0);
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(password, user.getPassword())) {
            return ResponseUtil.fail(AUTH_INVALID_ACCOUNT, "账号密码不对");
        }
        user.setTemperature(Float.valueOf(temperature));
        if(Float.valueOf(temperature).floatValue()>37.2) {
        	user.setIsRegular(Boolean.FALSE);
        }
        else {
        	user.setIsRegular(Boolean.TRUE);
        }
        	
        userService.updateById(user);
    	return ResponseUtil.ok(user);
    }
}