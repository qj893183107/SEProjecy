package org.seproject.litemall.admin.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.seproject.litemall.admin.service.LogHelper;
import org.seproject.litemall.admin.util.Permission;
import org.seproject.litemall.admin.util.PermissionUtil;
import org.seproject.litemall.core.util.IpUtil;
import org.seproject.litemall.core.util.JacksonUtil;
import org.seproject.litemall.core.util.RegexUtil;
import org.seproject.litemall.core.util.ResponseUtil;
import org.seproject.litemall.core.util.bcrypt.BCryptPasswordEncoder;
import org.seproject.litemall.db.domain.LitemallAdmin;
import org.seproject.litemall.db.service.LitemallAdminService;
import org.seproject.litemall.db.service.LitemallPermissionService;
import org.seproject.litemall.db.service.LitemallRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;

import static org.seproject.litemall.admin.util.AdminResponseCode.ADMIN_INVALID_ACCOUNT;
import static org.seproject.litemall.admin.util.AdminResponseCode.ADMIN_INVALID_NAME;
import static org.seproject.litemall.admin.util.AdminResponseCode.ADMIN_INVALID_PASSWORD;
import static org.seproject.litemall.admin.util.AdminResponseCode.ADMIN_NAME_EXIST;

@RestController
@RequestMapping("/admin/create")
@Validated
public class AdminCreateController{
    private final Log logger = LogFactory.getLog(AdminAuthController.class);

    @Autowired
    private LitemallAdminService adminService;
    @Autowired
    private LitemallRoleService roleService;
    @Autowired
    private LitemallPermissionService permissionService;
    @Autowired
    private LogHelper logHelper;
   
     /*
     * 注册管理员账号
     */
    @PostMapping("/signin")
    public Object signin(@RequestBody LitemallAdmin admin) {
    	System.out.println("进入注册状态");
        Object error = validate(admin);
        if (error != null) {
            return error;
        }

        String username = admin.getUsername();
        List<LitemallAdmin> adminList = adminService.findAdmin(username);
        if (adminList.size() > 0) {
            return ResponseUtil.fail(ADMIN_NAME_EXIST, "管理员已经存在");
        }

        String rawPassword = admin.getPassword();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(rawPassword);
        admin.setPassword(encodedPassword);
        adminService.add(admin);
        logHelper.logAuthSucceed("添加管理员", username);
        return ResponseUtil.ok(admin);
    }
    
    private Object validate(LitemallAdmin admin) {
        String name = admin.getUsername();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        if (!RegexUtil.isUsername(name)) {
            return ResponseUtil.fail(ADMIN_INVALID_NAME, "管理员名称不符合规定");
        }
        String password = admin.getPassword();
        if (StringUtils.isEmpty(password) || password.length() < 6) {
            return ResponseUtil.fail(ADMIN_INVALID_PASSWORD, "管理员密码长度不能小于6");
        }
        return null;
    }
}
